package ChessFX;

import javafx.geometry.Pos;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Kernael on 26/11/2015.
 */

public class Piece{

    public static enum Type {
        Pawn,
        Bishop,
        Knight,
        Rook,
        Queen,
        King
    }

    public Type type;
    public Position pos;
    public Position[]  moves;
    public ImageView imageView;
    public boolean isBlack  = false;
    public boolean hasMoved = false;

    public Piece(Type type, int x, int y, boolean isBlack) {
        this.type = type;
        this.isBlack = isBlack;
        initMovement();
        pos  = new Position(x, y);
        if (isBlack)
            imageView = new ImageView(blackImages.get(type));
        else
            imageView = new ImageView(whiteImages.get(type));
        updateImageView();
    }

    public void updateImageView(){
        imageView.setFitWidth(ChessBoard.FIELD_WIDTH);
        imageView.setFitHeight(ChessBoard.FIELD_HEIGHT);
    }

    private ArrayList<Position> checkLines(Position[][] lines, ArrayList<Position> movements, ChessBoard board) {
        for (int i = 0; i < lines.length; i++) {
            Position[] line = lines[i];
            loop: for (int ii = 0; ii < line.length; ii++) {
                Position move = new Position(pos.x + line[ii].x, pos.y + line[ii].y);
                switch (board.isValidMove(this, move)) {
                    case "ok":
                        movements.add(move);
                        break;
                    case "stop_next":
                        movements.add(move);
                        break loop;
                    case "stop":
                        break loop;
                }
            }
        }

        return movements;
    }

    private Position[] flipPositions(Position[] movements) {
        Position[] positions = new Position[movements.length];

        for (int i = 0; i < movements.length; i++) {
            positions[i] = new Position(movements[i].oppositeX().x, movements[i].oppositeY().y);
        }

        return positions;
    }
    private ArrayList<Position> checkMoves(ChessBoard board, Position[] mvs) {
        Position[] toCheck;

        if (board.isBlackTurn)
            toCheck = flipPositions(mvs);
        else
            toCheck = mvs;

        ArrayList<Position> movements = new ArrayList<>();

        for (int i = 0; i < toCheck.length; i++) {
            Position move = new Position(pos.x + toCheck[i].x, pos.y + toCheck[i].y);
            if (board.isValidMove(this, move) == "ok" ||
                    board.isValidMove(this, move) == "stop_next") {
                movements.add(move);
            }
        }

        return movements;
    }

    private ArrayList<Position> checkWhiteCastle(ChessBoard board, ArrayList<Position> movements) {
        if (hasMoved) { return movements; }

        if (canKingCastle(board)) {
            movements.add(new Position(pos.x + 2, pos.y));
        }
        if (canQueenCastle(board)) {
            movements.add(new Position(pos.x - 2, pos.y));
        }

        return movements;
    }

    public boolean canQueenCastle(ChessBoard board){
        if (hasMoved) { return false; }
        Piece queen_tower = board.pieces[pos.x - 4][pos.y];

        if (queen_tower == null) { return false; }

        if (queen_tower.type == Type.Rook) {
            if (!queen_tower.hasMoved) {
                if (board.pieces[pos.x - 1][pos.y] == null &&
                        board.pieces[pos.x - 2][pos.y] == null &&
                        board.pieces[pos.x - 3][pos.y] == null) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean canKingCastle(ChessBoard board) {
        if (hasMoved) { return false; }
        Piece king_tower  = board.pieces[pos.x + 3][pos.y];

        if (king_tower.type == Type.Rook) {
            if (!king_tower.hasMoved) {
                if (board.pieces[pos.x + 1][pos.y] == null &&
                        board.pieces[pos.x + 2][pos.y] == null) {
                    return true;
                }
            }
        }

        return false;
    }

    private ArrayList<Position> whitePawnMoves(ChessBoard board) {
        ArrayList<Position> movements = new ArrayList<>();

        if (pos.y - 1 >= 0) {
            if (board.pieces[pos.x][pos.y - 1] == null) {
                movements.add(new Position(0, -1));
            }
        }
        if (pos.x + 1 < 8 && pos.y - 1 >= 0) {
            if (board.pieces[pos.x + 1][pos.y - 1] != null &&
                    board.pieces[pos.x + 1][pos.y - 1].type != Type.King &&
                    board.pieces[pos.x + 1][pos.y - 1].isBlack != isBlack) {
                movements.add(new Position(1, -1));
            }
        }
        if (pos.x - 1 >= 0 && pos.y - 1 >= 0 ) {
            if (board.pieces[pos.x - 1][pos.y - 1] != null &&
                    board.pieces[pos.x - 1][pos.y - 1].type != Type.King &&
                    board.pieces[pos.x - 1][pos.y - 1].isBlack != isBlack) {
                movements.add(new Position(-1, -1));
            }
        }
        if (pos.y - 2 >= 0) {
            if (board.pieces[pos.x][pos.y - 2] == null && !hasMoved) {
                movements.add(new Position(0, -2));
            }
        }

        return movements;
    }

    private ArrayList<Position> blackPawnMoves(ChessBoard board) {
        ArrayList<Position> movements = new ArrayList<>();

        if (pos.y + 1 < 8) {
            if (board.pieces[pos.x][pos.y + 1] == null) {
                movements.add(new Position(0, -1));
            }
        }
        if (pos.x + 1 < 8 && pos.y + 1 <  8) {
            if (board.pieces[pos.x + 1][pos.y + 1] != null &&
                    board.pieces[pos.x + 1][pos.y + 1].type != Type.King &&
                    board.pieces[pos.x + 1][pos.y + 1].isBlack != isBlack) {
                movements.add(new Position(-1, -1));
            }
        }
        if (pos.x - 1 >= 0 && pos.y + 1 < 8) {
            if (board.pieces[pos.x - 1][pos.y + 1] != null &&
                    board.pieces[pos.x - 1][pos.y + 1].type != Type.King &&
                    board.pieces[pos.x - 1][pos.y + 1].isBlack != isBlack) {
                movements.add(new Position(1, -1));
            }
        }
        if (pos.y + 2 < 8) {
            if (board.pieces[pos.x][pos.y + 2] == null && !hasMoved) {
                movements.add(new Position(0, -2));
            }
        }

        return movements;
    }

    public Position[] getMovements(ChessBoard board) {
        ArrayList<Position> movements = new ArrayList<>();

        switch(type) {
            case Pawn:
                if (isBlack) {
                    movements = blackPawnMoves(board);
                } else {
                    movements = whitePawnMoves(board);
                }
                movements = checkMoves(board, movements.toArray(new Position[movements.size()]));
                break;
            case Knight:
                movements = checkMoves(board, moves);
                break;
            case King:
                movements = checkMoves(board, moves);
                break;
            case Bishop:
                movements = checkLines(MovementTable.diagonals, movements, board);
                break;
            case Rook:
                movements = checkLines(MovementTable.verticals, movements, board);
                movements = checkLines(MovementTable.horizontals, movements, board);
                break;
            case Queen:
                movements = checkLines(MovementTable.diagonals, movements, board);
                movements = checkLines(MovementTable.verticals, movements, board);
                movements = checkLines(MovementTable.horizontals, movements, board);
                break;
        }

        return movements.toArray(new Position[movements.size()]);
    }

    private void initMovement() {
        switch (type) {
            case Pawn:
                moves = MovementTable.pawnMoves();
                break;
            case Knight:
                moves = MovementTable.knightMoves();
                break;
            case King:
                moves = MovementTable.kingMoves();
                break;
        }
    }

    public void move(ChessBoard chessBoard, int x, int y){
        Piece[][] pieces = chessBoard.pieces;
        pieces[this.pos.x][this.pos.y] = null;
        this.pos = new Position(x, y);
        pieces[x][y] = this;
        chessBoard.pieceSelected = null;
        hasMoved = true;
    }

    private static HashMap<Type, String> blackImages = new HashMap<Type, String>(){{
        put(Type.Pawn,   "file:" + System.getProperty("user.dir") + "/src/ChessFX/res/blackPawn.png");
        put(Type.Bishop, "file:" + System.getProperty("user.dir") + "/src/ChessFX/res/blackBishop.png");
        put(Type.Knight, "file:" + System.getProperty("user.dir") + "/src/ChessFX/res/blackKnight.png");
        put(Type.Rook,   "file:" + System.getProperty("user.dir") + "/src/ChessFX/res/blackRook.png");
        put(Type.Queen,  "file:" + System.getProperty("user.dir") + "/src/ChessFX/res/blackQueen.png");
        put(Type.King,   "file:" + System.getProperty("user.dir") + "/src/ChessFX/res/blackKing.png");
    }};

    private static HashMap<Type, String> whiteImages = new HashMap<Type, String>(){{
        put(Type.Pawn,   "file:" + System.getProperty("user.dir") + "/src/ChessFX/res/whitePawn.png");
        put(Type.Bishop, "file:" + System.getProperty("user.dir") + "/src/ChessFX/res/whiteBishop.png");
        put(Type.Knight, "file:" + System.getProperty("user.dir") + "/src/ChessFX/res/whiteKnight.png");
        put(Type.Rook,   "file:" + System.getProperty("user.dir") + "/src/ChessFX/res/whiteRook.png");
        put(Type.Queen,  "file:" + System.getProperty("user.dir") + "/src/ChessFX/res/whiteQueen.png");
        put(Type.King,   "file:" + System.getProperty("user.dir") + "/src/ChessFX/res/whiteKing.png");
    }};
}
