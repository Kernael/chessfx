package ChessFX;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.HashMap;

import static ChessFX.Piece.Type;

public class ChessBoard extends GridPane {
    public static final int WIDTH = 8;
    public static final int HEIGHT = 8;
    public static double FIELD_WIDTH = ChessApplication.WIDTH / WIDTH;
    public static double FIELD_HEIGHT = ChessApplication.HEIGHT / HEIGHT;
    public static final Color LIGHT_SQUARE = Color.color((float) 240 / 255, (float) 217 / 255, (float) 181 / 255);
    public static final Color DARK_SQUARE = Color.color((float) 181 / 255, (float) 136 / 255, (float) 99 / 255);
    public Piece[][] pieces = new Piece[WIDTH][HEIGHT];
    public HashMap<Integer, Type> piecesPositions;
    public ChessBoard chessBoard;
    public boolean isBlackTurn = false;
    public Piece pieceSelected = null;

    public ChessBoard() {
        fillBoard();
        initPiecesPositions();
        initPieces();
        setOnMouseClicked(mouseEventEventHandler);
        chessBoard = this;
    }

    public void move(Piece piece, int x, int y) {
        for (Position movement : piece.getMovements(chessBoard)) {
            if (movement.x == x && movement.y == y) {
                piece.move(chessBoard, x, y);
                piece.hasMoved = true;
                redraw();
                turnBoard();
                isBlackTurn = !isBlackTurn;
            }
        }
        castleMoveChecker(piece, x, y);
        pieceSelected = null;
        redraw();
    }

    public void castleHandler(Piece piece) {
        if (piece.canKingCastle(chessBoard)) {
            Rectangle rectangle = new Rectangle(FIELD_WIDTH, FIELD_HEIGHT, Color.RED);
            rectangle.setOpacity(0.4);
            add(rectangle, piece.pos.x + 2, piece.pos.y);
        }
        if (piece.canQueenCastle(chessBoard)) {
            Rectangle rectangle = new Rectangle(FIELD_WIDTH, FIELD_HEIGHT, Color.RED);
            rectangle.setOpacity(0.4);
            add(rectangle, piece.pos.x - 2, piece.pos.y);
        }
    }

    public void castleMoveChecker(Piece piece, int x, int y) {
        if (piece.type == Type.King) {
            if ((x == piece.pos.x + 2 && y == piece.pos.y) && piece.canKingCastle(chessBoard)) {
                Piece rook = pieces[7][piece.pos.y];
                rook.move(chessBoard, rook.pos.x - 2, rook.pos.y);
                piece.move(chessBoard, x, y);
                redraw();
                turnBoard();
                isBlackTurn = !isBlackTurn;
            }
            if ((x == piece.pos.x - 2 && y == piece.pos.y) && piece.canQueenCastle(chessBoard)) {
                Piece rook = pieces[0][piece.pos.y];
                rook.move(chessBoard, rook.pos.x + 3, rook.pos.y);
                piece.move(chessBoard, x, y);
                redraw();
                turnBoard();
                isBlackTurn = !isBlackTurn;
            }
        }
    }

    public EventHandler<MouseEvent> mouseEventEventHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            int x = (int) (event.getX() / FIELD_WIDTH);
            int y = (int) (event.getY() / FIELD_HEIGHT);

            if (pieceSelected != null) {
                move(pieceSelected, x, y);
            } else {
                Piece piece;
                if ((piece = pieces[x][y]) != null) {
                    if (piece.isBlack == isBlackTurn) {
                        Position[] pieceMovements = piece.getMovements(chessBoard);
                        if (piece.type == Type.King)
                            castleHandler(piece);
                        pieceSelected = piece;
                        Rectangle rectangle = new Rectangle(FIELD_WIDTH, FIELD_HEIGHT);
                        rectangle.setFill(new Color((float) 57 / 255, (float) 128 / 255, (float) 49 / 255, 0.4));
                        add(rectangle, piece.pos.x, piece.pos.y);
                        for (Position movement : pieceMovements) {
                            Rectangle square = new Rectangle(FIELD_WIDTH, FIELD_HEIGHT);
                            square.setFill(new Color(0,0,0,0));
                            square.setStroke(new Color((float) 57 / 255, (float) 128 / 255, (float) 49 / 255, 1));
                            square.strokeWidthProperty().setValue(4);
                            add(square, movement.x, movement.y);
                        }
                    }
                }
            }
        }
    };

    public void initPiecesPositions() {
        piecesPositions = new HashMap<>();
        piecesPositions.put(0, Type.Rook);
        piecesPositions.put(1, Type.Knight);
        piecesPositions.put(2, Type.Bishop);
        piecesPositions.put(3, Type.Queen);
        piecesPositions.put(4, Type.King);
        piecesPositions.put(5, Type.Bishop);
        piecesPositions.put(6, Type.Knight);
        piecesPositions.put(7, Type.Rook);
    }

    private boolean collidePiece(Position move) {
        return pieces[move.x][move.y] != null;
    }

    private boolean outsideBoundaries(Position move) {
        return move.x >= WIDTH || move.y >= HEIGHT || move.x < 0 || move.y < 0;
    }

    public String isValidMove(Piece piece, Position move) {
        if (outsideBoundaries(move))
            return "stop";
        if (collidePiece(move))
            if (pieces[move.x][move.y].isBlack != piece.isBlack)
                return "stop_next";
            else
                return "stop";
        return "ok";
    }

    private void initPieces() {
        for (int col = 0; col < WIDTH; col++) {
            for (int row = 0; row < HEIGHT; row++) {
                switch (row) {
                    case 7:
                    case 0:
                        pieces[col][row] = new Piece(piecesPositions.get(col), col, row, row == 0);
                        add(pieces[col][row].imageView, col, row);
                        break;
                    case 6:
                    case 1:
                        pieces[col][row] = new Piece(Type.Pawn, col, row, row == 1);
                        add(pieces[col][row].imageView, col, row);
                        break;
                }
            }
        }
    }

    private void turnBoard() {
        double rotation = 180;
        if (isBlackTurn)
            rotation = 0;
        this.setRotate(rotation);
        for (int col = 0; col < WIDTH; col++) {
            for (int row = 0; row < HEIGHT; row++) {
                if (pieces[col][row] != null) {
                    pieces[col][row].imageView.setRotate(rotation);
                    pieces[col][row].updateImageView();
                }
            }
        }
    }

    private void redraw() {
        getChildren().clear();
        getColumnConstraints().clear();
        getRowConstraints().clear();
        fillBoard();
        for (int col = 0; col < WIDTH; col++) {
            for (int row = 0; row < HEIGHT; row++) {
                if (pieces[col][row] != null) {
                    pieces[col][row].updateImageView();
                    add(pieces[col][row].imageView, col, row);
                }
            }
        }
    }


    @Override
    public void resize(double width, double height) {
        FIELD_WIDTH = width / WIDTH;
        FIELD_HEIGHT = height / HEIGHT;
        redraw();
        super.resize(width, height);
    }

    private void fillBoard() {
        for (int col = 0; col < WIDTH; col++) {
            ColumnConstraints colConst = new ColumnConstraints();
            colConst.setPercentWidth(100.0 / WIDTH);
            getColumnConstraints().add(colConst);
        }

        for (int row = 0; row < HEIGHT; row++) {
            RowConstraints rowConst = new RowConstraints();
            rowConst.setPercentHeight(100.0 / HEIGHT);
            getRowConstraints().add(rowConst);
        }
        for (int row = 0; row < HEIGHT; row++) {
            for (int col = 0; col < WIDTH; col++) {
                if (col % 2 == row % 2) {
                    add(new Rectangle(FIELD_WIDTH, FIELD_HEIGHT, LIGHT_SQUARE), col, row);
                } else {
                    add(new Rectangle(FIELD_WIDTH, FIELD_HEIGHT, DARK_SQUARE), col, row);
                }
            }
        }
    }
}