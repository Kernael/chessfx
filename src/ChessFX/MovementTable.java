package ChessFX;

import java.util.ArrayList;

/**
 * Created by Kernael on 28/11/2015.
 */
public class MovementTable {

    public static Position[][] horizontals = createHorizontals();
    public static Position[][] verticals = createVerticals();
    public static Position[][] diagonals = createDiagonals();

    private static Position[][] createHorizontals() {
        ArrayList<Position[]> horizontals = new ArrayList<>();

        Position[] right = new Position[7];
        for (int i = 1; i < 8; i++) {
            right[i - 1] = new Position(i, 0);
        }
        horizontals.add(right);

        Position[] left = new Position[7];
        for (int i = 1; i < 8; i++) {
            left[i - 1] = new Position(-i, 0);
        }
        horizontals.add(left);

        return horizontals.toArray(new Position[2][7]);
    }

    private static Position[][] createVerticals() {
        ArrayList<Position[]> verticals = new ArrayList<>();

        Position[] up = new Position[7];
        for (int i = 1; i < 8; i++) {
            up[i - 1] = new Position(0, -i);
        }
        verticals.add(up);

        Position[] down = new Position[7];
        for (int i = 1; i < 8; i++) {
            down[i - 1] = new Position(0, i);
        }
        verticals.add(down);

        return verticals.toArray(new Position[2][7]);
    }

    private static Position[][] createDiagonals() {
        ArrayList<Position[]> diagonals = new ArrayList<>();

        Position[] bottomRight = new Position[7];
        for (int i = 1; i < 8; i++) {
            bottomRight[i - 1] = new Position(i, i);
        }
        diagonals.add(bottomRight);

        Position[] bottomLeft = new Position[7];
        for (int i = 1; i < 8; i++) {
            bottomLeft[i - 1] = new Position(-i, i);
        }
        diagonals.add(bottomLeft);

        Position[] upRight = new Position[7];
        for (int i = 1; i < 8; i++) {
            upRight[i - 1] = new Position(i, -i);
        }
        diagonals.add(upRight);

        Position[] upLeft = new Position[7];
        for (int i = 1; i < 8; i++) {
            upLeft[i - 1] = new Position(-i, -i);
        }
        diagonals.add(upLeft);

        return diagonals.toArray(new Position[4][7]);
    }

    public static Position[] pawnMoves() {
        return pawnTable;
    }

    public static Position[] knightMoves() {
        ArrayList<Position> moves = new ArrayList<>();
        for (Position aKnightTable : knightTable) {
            moves.add(aKnightTable);
            moves.add(aKnightTable.oppositeY());
        }

        return moves.toArray(new Position[moves.size()]);
    }

    public static Position[] kingMoves() {
        ArrayList<Position> moves = new ArrayList<>();

        for (Position[] diagonal : diagonals) {
            moves.add(diagonal[0]);
        }
        for (Position[] vertical : verticals) {
            moves.add(vertical[0]);
        }
        for (Position[] horizontal : horizontals) {
            moves.add(horizontal[0]);
        }

        return moves.toArray(new Position[moves.size()]);
    }

    private static Position[] pawnTable   = new Position[]{
            new Position(0, -1),
            new Position(0, -2),
            new Position(1, -1),
            new Position(-1, -1)
    };
    private static Position[] knightTable = new Position[]{
            new Position(1, 2),
            new Position(-1, 2),
            new Position(2, 1),
            new Position(-2, 1)
    };
}
