package ChessFX;

/**
 * Created by Kernael on 26/11/2015.
 */

public class Position {
    public int x, y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position oppositeX() { return new Position(-x, y); }
    public Position oppositeY() {
        return new Position(x, -y);
    }
}

