package ChessFX;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.lang.Exception;
import java.lang.Override;

public class ChessApplication extends Application {
    public static final int WIDTH  = 800;
    public static final int HEIGHT = 800;
    private ChessBoard board;

    @Override
    public void init() throws Exception {
        board  = new ChessBoard();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("ChessFX");
        primaryStage.setScene(new Scene(board, WIDTH, HEIGHT));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}